Source: fonts-sil-awami-nastaliq
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Bobby de Vos <bobby_devos@sil.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Homepage: https://software.sil.org/awami
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-awami-nastaliq.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-awami-nastaliq
Rules-Requires-Root: no

Package: fonts-sil-awami-nastaliq
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Smart Unicode font for the Nastaliq script
 Awami Nastaliq is a Nastaliq-style Arabic script font supporting a wide variety
 of languages of Southwest Asia, including but not limited to Urdu. This font
 is aimed at minority language support. This makes it unique among Nastaliq
 fonts.
 .
 Awami means "of the people", "of the common population" or "public".
 .
 The Awami Nastaliq font does not provide complete coverage of all the
 characters defined in Unicode for Arabic script. Because the font style is
 specifically intended for languages using the Nastaliq style of southwest Asia,
 the character set for this font is aimed at those languages.
 .
 This font makes use of state-of-the-art font technologies to support complex
 typographic issues. Font smarts have been implemented using Graphite only.
 There are no current plans to support OpenType.
 .
 One font from this typeface family is included in this release:
     * Awami Nastaliq Regular
 .
 Webfont versions and HTML/CSS examples are also available.
 .
 The full font sources are publicly available at
 https://github.com/silnrsi/font-awami
 An open workflow is used for building, testing and releasing.
